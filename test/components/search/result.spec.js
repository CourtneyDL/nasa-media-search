import React from 'react';
import $ from 'jquery';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import { Result } from '../../../source/js/components/Search/Result';

describe('component/search/Result', function () {
    beforeEach(() => {
        $.fn.tooltip = () => { };
    });

    describe('should be in its base state', function () {
        beforeEach(() => {
            $.fn.tooltip = () => { };
        });
        
        it('with no thumbnail class', function () {
            const wrapper = shallow(<Result/>);
            const obj_thumb = wrapper.find('div.nasa-search-result-thumb');
            expect(obj_thumb).to.have.length(1);
            expect(obj_thumb.hasClass('no-thumb')).to.equal(true);
        });

        it('with no thumbnail img', function () {
            const wrapper = shallow(<Result/>);
            const obj_img = wrapper.find('div.nasa-search-result-thumb > img');
            expect(obj_img).to.have.length(0);
        });
    });

    describe('should be a video', function () {
        beforeEach(() => {
            $.fn.tooltip = () => { };
        });

        const obj_data = {
            id: "NHQ_2018_0131_Super Blue Moon Lunar Eclipse",
            media_type: "video",
            title: "Super Blue Moon Lunar Eclipse",
            description: "*A Description*",
            collection: "https://images-assets.nasa.gov/video/NHQ_2018_0131_Super Blue Moon Lunar Eclipse/collection.json",
            thumbnail: "http://images-assets.nasa.gov/video/NHQ_2018_0131_Super Blue Moon Lunar Eclipse/NHQ_2018_0131_Super Blue Moon Lunar Eclipse~preview_thumb_00001.png",
        };
        const arr_history = [];

        it('without a no thumbnail class', function () {
            const wrapper = mount(<Result history={arr_history} data={obj_data}/>);
            const obj_thumb = wrapper.find('div.nasa-search-result-thumb');
            expect(obj_thumb).to.have.length(1);
            expect(obj_thumb.hasClass('no-thumb')).to.equal(false);
        });

        it('with the expected tooltip title', function () {
            const wrapper = mount(<Result history={arr_history} data={obj_data}/>);
            const obj_thumb = wrapper.find('div.nasa-search-result-thumb');
            expect(obj_thumb.prop('title')).to.equal(obj_data.title);
        });

        it('with the correct thumbnail', function () {
            const wrapper = mount(<Result history={arr_history} data={obj_data}/>);
            const obj_img = wrapper.find('div.nasa-search-result-thumb > img');
            expect(obj_img).to.have.length(1);
            expect(obj_img.prop('src')).to.equal(obj_data.thumbnail);
        });
        
        it('with a play icon', function () {
            const wrapper = mount(<Result history={arr_history} data={obj_data}/>);
            const obj_icon = wrapper.find('div.nasa-search-result-thumb > .play-icon');
            expect(obj_icon).to.have.length(1);
            expect(obj_icon.prop('name')).to.equal('play-circle');
        });

        it('and navigates to the correct asset when clicked', function () {
            const wrapper = mount(<Result history={arr_history} data={obj_data}/>);
            const obj_thumb = wrapper.find('div.nasa-search-result-thumb');
            obj_thumb.simulate('click');
            wrapper.update();
            expect(wrapper.prop('history')).to.include(`/asset/${obj_data.id}`);
        });
    });
});