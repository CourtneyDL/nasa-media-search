# NASA Media Search
### Single Page React/Redux App developed by Courtney Leacock

This project is an for searching NASA's library of images, video and audio content via the NASA Image and Video Library API.

The application was developed using React(15.5.4), Redux, Bootstrap 4, Sass and React Router v4 setup with a Webpack 2 build process. 

The codebase is derived from a boilerplate called [Marvin](https://github.com/workco/marvin/tree/d6d426243f02198a5bfaead181b4e72e0e1cbcd4). I used this for a previous project upon which this one is based.


The [Plyr](https://github.com/sampotts/plyr) media player library is used for video and audio playback.

The icon set is from [Font Awesome](https://fontawesome.com/).

##Build Process

### Install dependences
```
npm i
```

A hot loading development version of the application can be started by running the following command:
```
npm start
```

#### Basic build
```
npm run build
```

#### Local Build
```
npm run build:local
```

This build is intended to be hosted at http://localhost:4000/build using a server started from the root directory of the project such as a simple Python 2 HTTP server.
```
python -m SimpleHTTPServer 4000
```
This command doesn't set up any server side routing so reloading the page will result in a 404.

### Bitbucket Pages Build

```
npm run build:bitbucket
```
This build is hosted at https://courtneydl.bitbucket.io/nasa-media-search using Bitbucket pages.

## Items Requiring Further Development

### Full Test Suite
In this version there is a sample set of tests for the Search Result component (components/Search/Result). This is a proof of concept for automated unit testing using Mocha, Chai and Enzyme. I have used Mocha and Chai for automated NodeJS unit testing previous but my familiarity with it is limited. I have used this project as opportunity to reaccquaint myself with the techniques required and how to apply them to React in the front end which I have not done previously.  

The test directory is structured to mirror the source/js directory for any components or classes tested. The current is shown below:

```
├── test
    └──components
        └──search
            └──result.spec.js
```

This can run once using:
```
npm test
```
or with a watch enabled
```
npm run test:watch
```

With more time I would expand the number of components tested and any further development would be subject to a TDD approach. I would also refactor the Search and Asset views to break them down into testable components e.g. the results container in the Search view.

I would also include tests for NASAMediaAPIClient class to ensure it is handling responses correctly. This would neccessitate finding a way to spoof API responses to run the code as it stands. Alternatively I'd only test functions that take an API response as a parameter and test them by provide sample data. Further development of this class would also be subject to a TDD approach.

### General Improvements
- **Update test watch command to watch for changes in the source files as well as the test scripts. (watch-run?)**
- Video/Audio playback - Allow users to select the file size they wish to view i.e. small, medium, large or original. This would allow the user to switch if their connection is not quick enough for default selection.
- Image viewer with pan, zoom and full screen capability.
- Smoother loading image both thumbnails and full size asset images.
- Add a Redux Saga to handle the search process including updating the URL.
- Add the ability to trigger a search when the media types are changed. Users currently have to press Search after filtering. The saga mentioned previously would streamline the implementation of this feature.
- Get routing working when hosted from GitHub or Bitbucket pages. (The latter may not be possible due to lack of 404.html support) Otherwise with the current routing solution, refreshing the page results in a 404 error without corresponding server side routing in place.
- Improve the UI as it is very much a prototype - revise the color scheme, fonts, introduce transitions, hover effect etc.