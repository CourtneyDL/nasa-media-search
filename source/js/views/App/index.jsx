import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import Search from 'views/Search';
import Asset from 'views/Asset';

import Navbar from 'components/Base/Navbar';

const public_path = '/';
let base_name = '/';
if (PUBLIC_URL && PUBLIC_URL !== base_name) {
    base_name = PUBLIC_URL.replace(location.origin,'');
}

export const route_codes = {
    SEARCH: `${public_path}search`,
    ASSET: `${ public_path }asset/:id`
};

export default class App extends Component {
    static propTypes = {
        children: PropTypes.object,
    }

    render() {
        return (
            <BrowserRouter basename={base_name}>
                <div>
                    <Navbar/>
                    <div className='nasa-container'>
                        <Switch>
                            <Route exact path={ route_codes.SEARCH } component={ Search } />
                            <Route path={ route_codes.ASSET } component={ Asset } />
                            <Redirect to={ route_codes.SEARCH }/>
                        </Switch>
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}
