import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';

import { startAssetLoad } from 'redux-state/actions/asset';

import LoadingIcon from 'components/Base/LoadingIcon';
import MediaPlyr from 'components/Asset/MediaPlyr';

class Asset extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        data: PropTypes.object,

        startAssetLoad: PropTypes.func
    }

    icons = {
        'audio': 'volume-up',
        'image': 'image',
        'video': 'film',
        'play': 'play-circle'
    }

    componentDidMount () {
        const { id } = this.props.match.params;
        if (id) {
            this.props.startAssetLoad(id);
        }
    }

    render() {
        const { loading, data={}, last_query } = this.props;
        const { title='', media_type= '', description = '', items=[] } = data || {};

        let str_media_url;
        switch(media_type) {
            case 'audio':
                str_media_url = items.find(item => /~128k.mp3/.test(item));
                break;
            case 'video':
                str_media_url = items.find(item => /~large.mp4/.test(item));
                break;
            case 'image':
                str_media_url = items.find(item => /~large/.test(item));
                break;
        }
        if (!str_media_url) {
            str_media_url = items[0];
        }

        return (
            <div className='nasa-asset-container'>
                <div className="nasa-asset-heading">
                    <div className="nasa-asset-title">
                        <h3><FontAwesome className={`icon ${typeof this.icons[media_type] === 'undefined' ? 'd-inline-block' : 'd-inline'}`} spin={typeof this.icons[media_type] === 'undefined'} name={this.icons[media_type] || 'spinner'}/>{title || 'Loading...'}</h3>
                    </div>
                    <NavLink
                        activeClassName='btn btn-primary active'
                        className='btn btn-primary'
                        to={ `/` }
                        >{`Go ${last_query ? 'Back' : 'to Search'}`}</NavLink>
                </div>
                <div>
                    { 
                        loading ?
                            <LoadingIcon/>
                            :
                            <div>
                                <div className="nasa-asset-media">
                                    {media_type === 'image' && str_media_url && <img src={str_media_url} alt={title || 'Asset'}/>}
                                    {['video','audio'].includes(media_type) && 
                                        <MediaPlyr src={str_media_url} title={title} type={media_type}/>
                                    }
                                </div>
                                <p>{description}</p>
                            </div>
                    }
                </div>
            </div>
        );
    }
}

export default connect(
    state => {
        const search_state = state.search.toJS(),
            asset_state  = state.asset.toJS();

        return {
        loading   : asset_state.loading,
        data      : asset_state.data,
        last_query: search_state.last_query
        }
    },
    dispatch => ({
        startAssetLoad: (id) => dispatch(startAssetLoad(id))
    })
)(Asset);