import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { performSearch, updateSearchString, updateSearchMediaTypes } from 'redux-state/actions/search';
import { resetAsset } from 'redux-state/actions/asset';

import Result from 'components/search/Result';
import PageNav from 'components/search/PageNav';
import SearchInput from 'components/search/SearchInput';
import SearchMediaTypes from 'components/search/SearchMediaTypes';
import LoadingOverlay from 'components/search/LoadingOverlay';

const RESULTS_PER_PAGE = 100;

class Search extends Component {
    static propTypes = {
        query: PropTypes.string,
        results: PropTypes.array,
        loading: PropTypes.bool,
        page_number: PropTypes.number,
        result_count: PropTypes.number,
        last_query: PropTypes.string,

        performSearch: PropTypes.func,
        updateSearchString: PropTypes.func,
        resetAsset: PropTypes.func,
        updateSearchMediaTypes: PropTypes.func
    }

    constructor() {
        super();
    }

    parseQueryString (search = this.props.location.search) {
        if (typeof search === 'string' && search.length > 0) { 
            search = search.substring(1);
            return JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}', function(key, value) { return key === "" ? value : decodeURIComponent(value); })
        }

        return {};
    }

    componentDidMount () {
        let { q, page='1', media_types } = this.parseQueryString();
        
        this.props.resetAsset();
        
        if (typeof q === "string") {
            if (media_types) {
                const media_types_array = media_types.split(',');
                media_types = {
                    image: media_types_array.includes('image'),
                    video: media_types_array.includes('video'),
                    audio: media_types_array.includes('audio')
                };
            }

            this.props.updateSearchString(q.replace(/\+/g,' '));
            if (media_types) {
                this.props.updateSearchMediaTypes(media_types);
            }
            this.performSearch(parseInt(page,10));
        }
    }

    onSearchInputChange(e) {
        this.props.updateSearchString(e.currentTarget.value);
    }

    onSearchInputKeyUp(e) {
        if (e.keyCode === 13) {
        this.performSearch();
        }
    }

    onSearchButtonClick() {
        if (this.props.query) {
            this.performSearch();
        }

        //DEBUG - hoc ref test
        console.log(`onSearchButtonClick - hoc test - input`, this.input);
        if (this.input) {
            console.log(`onSearchButtonClick - hoc test - has input`);
            this.input.logTest();
        }
    }

    
    onPageChange(page) {
        this.performSearch(page);
    }

    performSearch(page = 1) {
        if (typeof page !== 'number') {
            page = 1;
        }
        this.props.performSearch(page);
    }

    render() {
        const {
            query, loading, page_number, result_count, results=[], last_query
        } = this.props;

        return (
            <div className='nasa-search'>
                <div className="nasa-search-input">
                    <SearchInput value={query} disabled={loading}
                        withRef={elem => this.input=elem}
                        onClick={this.onSearchButtonClick.bind(this)} 
                        onChange={this.onSearchInputChange.bind(this)}
                        onKeyUp={this.onSearchInputKeyUp.bind(this)}/>
                    <SearchMediaTypes/>
                </div>
                <div className="nasa-search-results-container">
                    {loading && <LoadingOverlay display_background={typeof last_query == "string" && last_query.length > 0}/>}
                    {last_query && <h3 className="nasa-search-results-count">{`${result_count} result${result_count === 1 ? '' : 's'} for "${last_query}"`}</h3>}
                    <div className="nasa-search-results">
                        {results.map((result,index) => <Result key={index} data={result}/>)}
                        {result_count > 0 && results.length === 0 && <h4>There are no results for this page number (that's embarrassing)</h4>}
                    </div>
                </div>
                <PageNav disabled={loading} current_page={page_number} count={result_count} per_page={RESULTS_PER_PAGE} onPageChange={this.onPageChange.bind(this)}/>
            </div>
        );
    }
}

export default connect(
    state => {
        const search_state = state.search.toJS();

        return {
            query       : search_state.query,
            results     : search_state.results,
            loading     : search_state.loading,
            result_count: search_state.result_count,
            page_number : search_state.page_number,
            last_query  : search_state.last_query,
            media_types : search_state.media_types
        }
    },
    dispatch => ({
        updateSearchString: s => dispatch(updateSearchString(s)),
        performSearch: page => dispatch(performSearch(page)),
        resetAsset: () => dispatch(resetAsset()),
        updateSearchMediaTypes: ({ image=true, video=true, audio=true }) => dispatch(updateSearchMediaTypes({ image, video, audio }))
    })
)(Search);