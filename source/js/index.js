import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'emotion-theming';
import 'babel-polyfill';
import 'bootstrap';

import App from 'views/App';
import store from 'redux-state/store';

// Load SCSS
import '../scss/main.scss'; 

const theme = {
    textColor: 'red',
};

// Render it to DOM
ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={ store }>
            <App />
        </Provider>
    </ThemeProvider>,
    document.getElementById('root')
);
