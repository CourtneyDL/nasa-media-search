import React from 'react';

const withRef = () => WrappedComponent =>
	class withRef extends React.Component {
        render() {
			return <WrappedComponent ref={this.props.withRef} testPropRef={Date.now()} {...this.props} />;
		}
	};

export default withRef;