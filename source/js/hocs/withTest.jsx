import React from 'react';

const withTest = () => WrappedComponent =>
	class WithTest extends React.Component {
        render() {
			return <WrappedComponent testProp={Date.now()} {...this.props} />;
		}
	};

export default withTest;