import _collection from 'lodash/collection';

export const convertObjectToArray = media_types => 
    _collection.reduce(media_types, (arr, value, type) => {
        if (value === true) {
            arr.push(type);
        }
        return arr;
    }, []);


export default {
    convertObjectToArray
};