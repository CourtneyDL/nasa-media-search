//Enable/disable Bootstrap 4 Tooltip library - relies on jQuery
import $ from 'jquery';//TODO Revise how this is imported

const enable = elem => $(elem).tooltip();

const disable = elem => $(elem).tooltip('dispose');

export default {
    enable,
    disable
};