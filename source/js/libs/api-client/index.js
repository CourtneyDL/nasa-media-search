import _collection from 'lodash/collection';
import _object from 'lodash/object';
import axios from 'axios';

const api_url = 'https://images-api.nasa.gov';

const _search = (obj_options) => {
    let query_string = '';
    if (obj_options.id) {
      query_string = `?nasa_id=${obj_options.id}`;
    } else {
      query_string = _collection.reduce(obj_options, (query_string, value, prop) => {
        let param = '';
        switch(prop) {
          case 'media_type':
            if (Array.isArray(value) && value.length > 0) {
              param = `&media_type=${value.join()}`;
            }
            break;
          default:
            param = `${prop}=${value}`;
            break;
        }
        return param ? `${query_string}${query_string.length > 0 ? '&' : '?'}${param}` : query_string;
      }, '');
    }

    return _get(`/search${query_string}`)
      .then(processSearchResults.bind(this));
  }
  
const search = (q, page = 1, media_type=[]) => {
    return _search({ q, page, media_type })
        .then(({ total_count, items }) => Promise.all([ total_count, getThumbnails(items) ]))
        .then(([total_count, items]) => ({ total_count, items }))
        .catch(e => {
            alert("An error has occurred");
        });
}

const processSearchResults = (data) => {
    const items = _object.get(data, 'collection.items', []).map(item => {
        const { links=[] } = item;
        const thumbnail = (links.find(link => link.render === 'image' && link.rel === 'preview') || {}).href;

        return {
            id         : _object.get(item, 'data[0].nasa_id'),
            media_type : _object.get(item, 'data[0].media_type'),
            title      : _object.get(item, 'data[0].title'),
            description: _object.get(item, 'data[0].description'),
            collection : item.href,
            thumbnail
        };
    });

    return {
        total_count: _object.get(data, 'collection.metadata.total_hits', 0),
        items
    }
}

const getThumbnails = (items = []) => {
    return Promise.all(items.map(item => {
        if (item.thumbnail || item.media_type === 'audio' || !item.collection) {
            return item;
        }

        return axios.get(item.collection)
            .then(({ data:collection=[] }) => {
            item.thumbnail = collection.find(asset => /preview_thumb/.test(asset));
            })
            .catch(e => {})//Fail silently
            .then(() => item);//Always return an item
    }));
}

const _asset = (id) => _get(`/asset/${id}`);

const asset = (id) => {
    return Promise.all([_asset(id), _search({ id })])
        .then(([asset, { items }]) => {
            return { 
                ...items[0],
                items: _object.get(asset, 'collection.items', []).map(item => item.href)
            }
        })
        .catch(e => {
            alert("An error has occurred");
        });
}

const _get = endpoint => {
    return axios.get(`${api_url}${endpoint}`)
        .then(response => response.data);
}

export default {
    asset,
    search
};