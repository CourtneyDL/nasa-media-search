import _lang from 'lodash/lang';
import { takeEvery, put, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import api from 'libs/api-client';
import mediaTypes from 'libs/media-types';

import { SEARCH_LOAD_START, SEARCH_LOAD_COMPLETE, searchComplete } from 'redux-state/actions/search';

export function* watchSearch() {
    yield takeEvery(SEARCH_LOAD_START, makeSearchQuery);
    yield takeEvery(SEARCH_LOAD_COMPLETE, updateURL);
}

const getSearchState = state => state.search.toJS();

export function* makeSearchQuery ({ payload:page=-1 }) {
    const { query, last_query, media_types, last_media_types } = yield select(getSearchState);

    if (!query) {
        return;
    }

    if ((last_query && query !== last_query)
        || last_media_types && !_lang.isEqual(last_media_types, media_types)
        || page === -1) {
        page = 1;
    } else if (page === -1) {
        page = page_number
    }

    const media_types_array = mediaTypes.convertObjectToArray(media_types);

    try {
        const data = yield api.search(query, page, media_types_array);
        yield put(searchComplete(data, page));
    } catch (e) {
        alert('An error occurred performing this search');
    }
}

export function* updateURL () {
    let { last_query:q, page_number:page, last_media_types:media_types } = yield select(getSearchState);

    if (q) {
        q = q.replace(/\s/g,'+');

        const arr_media_types = mediaTypes.convertObjectToArray(media_types);

        yield put(push(`/search?q=${q}&page=${page}${arr_media_types.length > 0 ? `&media_types=${arr_media_types.join()}` : ''}`));
    }
}