import { watchAsset } from 'redux-state/sagas/asset';
import { watchSearch } from 'redux-state/sagas/search';

export default function* rootSaga () {
    yield [
        watchAsset(),
        watchSearch()
    ]
}