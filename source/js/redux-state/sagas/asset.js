import { takeEvery, put } from 'redux-saga/effects';

import api from 'libs/api-client';

import { ASSET_LOAD_START, assetLoadComplete } from 'redux-state/actions/asset';

export function* watchAsset() {
    yield takeEvery(ASSET_LOAD_START, loadAsset);
}

export function* loadAsset ({ payload:id='' }) {
    try {
        const data = yield api.asset(id);
        yield put(assetLoadComplete(data));
    } catch (e) {
        alert('The asset could not be loaded.');
    }
}