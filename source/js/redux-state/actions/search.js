import { createAction } from 'redux-actions';

export const SEARCH_STRING_UPDATE = 'SEARCH_STRING_UPDATE';
export const updateSearchString = createAction(SEARCH_STRING_UPDATE, query => query);

export const SEARCH_MEDIA_TYPE_UPDATE = 'SEARCH_MEDIA_TYPE_UPDATE';
export const updateSearchMediaTypes = createAction(SEARCH_MEDIA_TYPE_UPDATE, types => types);

export const SEARCH_LOAD_START = 'SEARCH_LOAD_START';
export const performSearch = createAction(SEARCH_LOAD_START, page => page);

export const SEARCH_LOAD_COMPLETE = 'SEARCH_LOAD_COMPLETE';
export const searchComplete = createAction(SEARCH_LOAD_COMPLETE, (data,page) => ({ data, page }));
