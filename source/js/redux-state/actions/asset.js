import { createAction } from 'redux-actions';

export const ASSET_RESET = 'ASSET_RESET';
export const resetAsset = createAction(ASSET_RESET);

export const ASSET_LOAD_START = 'ASSET_LOAD_START';
export const startAssetLoad = createAction(ASSET_LOAD_START, id => id);

export const ASSET_LOAD_COMPLETE = 'ASSET_LOAD_COMPLETE';
export const assetLoadComplete = createAction(ASSET_LOAD_COMPLETE, data => data);
