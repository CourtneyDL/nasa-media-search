import { Map } from 'immutable';
import _ from 'lodash/object';

import {
    ASSET_RESET,
    ASSET_LOAD_START,
    ASSET_LOAD_COMPLETE,
    ASSET_README_START,
    ASSET_README_COMPLETE
} from 'redux-state/actions/asset';

const initialState = Map({
    loading: false,
    data: Map({}),
    readme_loading: false,
    readme: ''
});

const actionsMap = {
    [ASSET_RESET]: (state,action) => {
        return state.merge({
            readme_loading: false,
            loading: false,
            data: {},
            readme: ''
        })
    },
    [ASSET_LOAD_START]: (state,action) => {
        return state.merge({
            loading: true
        })
    },
    [ASSET_LOAD_COMPLETE]: (state,action) => {
        return state.merge({
            loading: false,
            data: _.get(action, 'payload', {}),
        })
    },
    [ASSET_README_START]: (state,action) => {
        return state.merge({
            readme_loading: true
        })
    },
    [ASSET_README_COMPLETE]: (state,action) => {
        return state.merge({
            readme_loading: false,
            readme: _.get(action, 'payload', ''),
        })
    }
};

export default function reducer(state = initialState, action = {}) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action) : state;
}
