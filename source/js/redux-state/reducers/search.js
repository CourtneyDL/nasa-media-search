import { Map, List } from 'immutable';
import _ from 'lodash/object';

import {
    SEARCH_LOAD_START,
    SEARCH_LOAD_COMPLETE,
    SEARCH_STRING_UPDATE,
    SEARCH_MEDIA_TYPE_UPDATE
} from 'redux-state/actions/search';

const initialState = Map({
    page_number: 1,
    result_count: 0,
    results: List([]),
    loading: false,
    query: '',
    last_query: '',
    media_types: {
        image: true,
        video: true,
        audio: true
    },
    last_media_types: null
});

const actionsMap = {
    [SEARCH_STRING_UPDATE]: (state,action) => {
        return state.merge({
            query: _.get(action, 'payload', ''),
        })
    },
    [SEARCH_MEDIA_TYPE_UPDATE]: (state,action) => {
        return state.merge({
            media_types: action.payload,
        })
    },
    [SEARCH_LOAD_START]: (state) => {
        return state.merge({
            loading:true
        });
    },
    [SEARCH_LOAD_COMPLETE]: (state,action) => {
        return state.merge({
            result_count: _.get(action, 'payload.data.total_count', 0),
            loading:false,
            results: _.get(action, 'payload.data.items', []),
            page_number: _.get(action, 'payload.page', 1),
            last_query: state.get('query'),
            last_media_types: state.get('media_types')
        });
    }
};

export default function reducer(state = initialState, action = {}) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action) : state;
}
