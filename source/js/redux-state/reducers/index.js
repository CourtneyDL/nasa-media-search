import { combineReducers } from 'redux';
import asset from 'redux-state/reducers/asset';
import search from 'redux-state/reducers/search';

export default combineReducers({
    asset,
    search
});
