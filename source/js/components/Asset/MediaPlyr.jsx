import React, { Component } from 'react';
import plyr from 'plyr';
import PropTypes from 'prop-types';

export default class MediaPlyr extends Component {
    static propTypes = {
        src: PropTypes.string,
        type: PropTypes.string,
        title: PropTypes.string
    }

    componentDidMount () {
        if (this.target_elem) {
            this.player = plyr.setup([this.target_elem]);
        }
    }

    componentWillUnmount() {
        if (this.player.length > 0) {
            for (const player_elem of this.player) {
                player_elem.destroy();
            }
        }
    }

    render() {
        const { type, src } = this.props;

        return (
            <div className="nasa-asset-player">
                {type === 'video' &&
                    <video ref={elem => this.target_elem = elem} playsInline controls>
                        <source src={src} type="video/mp4" />
                    </video>
                }
                {type === 'audio' &&
                    <audio ref={elem => this.target_elem = elem} controls>
                        <source src={src} type="audio/mp3" />
                    </audio>
                }
            </div>
        );
    }
}