import React, { Component } from 'react';
import _collection from 'lodash/collection';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { updateSearchMediaTypes, performSearch } from 'redux-state/actions/search';

class SearchMediaTypes extends Component {
    static propTypes = {
        media_types: PropTypes.object,
        disabled: PropTypes.bool,
        
        updateSearchMediaTypes: PropTypes.func,
        performSearch: PropTypes.func
    }

    constructor() {
        super();
    }

  
    onChange(e) {
        const { media_types } = this.props;
        const { name, checked } = e.currentTarget;

        media_types[name] = checked;
        this.props.updateSearchMediaTypes(media_types);
        this.props.performSearch();
    }

    render () {
        const { media_types, disabled=false, onChange } = this.props;

        //convert media types object to array
        const arr_media_types = _collection.reduce(media_types, (arr, value, type) => {
            if (value === true) {
                arr.push(type);
            }
            return arr;
        }, []);

        const image_disabled = disabled || arr_media_types.length === 1 && media_types.image === true;
        const video_disabled = disabled || arr_media_types.length === 1 && media_types.video === true;
        const audio_disabled = disabled || arr_media_types.length === 1 && media_types.audio === true;

        return (
            <div>
                <div className="form-check form-check-inline">
                <input className="form-check-input" type="checkbox" name="image" checked={media_types.image} disabled={image_disabled} onChange={this.onChange.bind(this)}/>
                <label className="form-check-label" htmlFor="image">Image</label>
                </div>
                <div className="form-check form-check-inline">
                <input className="form-check-input" type="checkbox" name="video" checked={media_types.video} disabled={video_disabled} onChange={this.onChange.bind(this)}/>
                <label className="form-check-label" htmlFor="video">Video</label>
                </div>
                <div className="form-check form-check-inline">
                <input className="form-check-input" type="checkbox" name="audio" checked={media_types.audio} disabled={audio_disabled} onChange={this.onChange.bind(this)}/>
                <label className="form-check-label" htmlFor="audio">Audio</label>
                </div>
            </div>
        )
    }
}

export default connect(
    state => {
        const { media_types, loading:disabled } = state.search.toJS();

        return { media_types, disabled };
    },
    dispatch => ({
        updateSearchMediaTypes: ({ image=true, video=true, audio=true }) => dispatch(updateSearchMediaTypes({ image, video, audio })),
        performSearch: () => dispatch(performSearch())
    })
)(SearchMediaTypes);