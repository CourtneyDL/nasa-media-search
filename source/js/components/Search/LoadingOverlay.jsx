import React, { Component } from 'react';
import PropTypes from 'prop-types';

import LoadingIcon from 'components/Base/LoadingIcon';

export default class LoadingOverlay extends Component {
    static propTypes = {
        display: PropTypes.bool,
        display_background: PropTypes.bool
    }

    constructor() {
        super();
    }

    render () {
        return (
            <div className={`nasa-loading-overlay ${this.props.display_background ? ' include-bg' : ''}`}>
                <LoadingIcon/>
            </div>
        );
    }
}