import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';

import tooltip from 'libs/tooltip';

export class Result extends Component {

    static propTypes = {
        data: PropTypes.object
    }

    icons = {
        'audio': 'volume-up',
        'image': 'image',
        'video': 'film',
        'play': 'play-circle'
    }

    onClick (id) {
        this.props.history.push(`/asset/${this.props.data.id}`);
    }

    componentDidMount () {
        tooltip.enable(this.tile_elem);
    }

    componentWillUnmount () {
        tooltip.disable(this.tile_elem);
    }

    render() {
        const { title, media_type, thumbnail } = this.props.data || {};

        return (
            <div className="nasa-search-result">
                <div className={`nasa-search-result-thumb${thumbnail ? '' : ' no-thumb'}`} onClick={this.onClick.bind(this)}
                    data-toggle="tooltip" data-placement="bottom" title={title}
                    ref={elem => this.tile_elem = elem}>
                    {thumbnail ?
                        <img src={thumbnail}/>
                        :
                        <FontAwesome className="icon" name={this.icons[media_type] || 'unlink'}/>}
                    {media_type === 'video' && <FontAwesome className="play-icon" name={this.icons.play}/>}
                </div>
            </div>
        )
    }
}

export default withRouter(Result);