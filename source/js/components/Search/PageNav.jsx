import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PageNav extends Component {
    static propTypes = {
        current_page: PropTypes.number,
        count: PropTypes.number,
        per_page: PropTypes.number,
        onPageChange: PropTypes.func,
        disabled: PropTypes.bool
    };

    onPageClick(e) {
        const { current_page, onPageChange, disabled } = this.props;
        const new_current_page = parseInt(e.currentTarget.getAttribute('data-page'),10);

        if (!disabled && current_page !== new_current_page) {
            onPageChange(new_current_page)
        }
    }

    onPreviousClick() {
        const { current_page, onPageChange, disabled } = this.props;

        if (!disabled && current_page > 1) {
            onPageChange(current_page - 1);
        }
    }

    onNextClick() {
        const {current_page, count, per_page, onPageChange, disabled} = this.props
        const page_count = Math.ceil(count / per_page)
        
        if (!disabled && current_page < page_count) {
        onPageChange(current_page + 1);
        }
    }

    render() {
        const {current_page, count, per_page, disabled} = this.props

        const page_count = Math.ceil(count / per_page)

        if (page_count < 2) {
            return null;
        }

        let start = current_page - 2;
        let end = current_page + 2;
        if (start <= 0) {
            end -= start - 1;
            start = 1;
        }
        if (end > page_count) {
            start -= end - page_count;
            end = page_count;
        }
        start = start <= 0 ? 1 : start;

        let li = [];
        li.push(
            <li className={`page-item${(current_page === 1 || disabled) ? ' disabled' : ''}`} key="prev">
                <a className="page-link" href="#" aria-label="Previous" onClick={this.onPreviousClick.bind(this)}>
                <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        );
        
        for (let i = start; i <= end; i += 1) {
            const active = current_page === i;
            li.push(
                <li key={i} className={`page-item ${disabled && !active ? 'disabled' : ''} ${active ? 'active' : ''}`}>
                    <a className="page-link" href="#" data-page={i} onClick={this.onPageClick.bind(this)}>{i}</a>
                </li>
            );
        }

        li.push(
            <li className={`page-item ${(current_page === page_count || disabled) ? 'disabled' : ''}`} key="next">
                <a className="page-link" href="#" aria-label="Next" onClick={this.onNextClick.bind(this)}>
                <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        );

        return (
            <nav className="navbar fixed-bottom justify-content-center bg-light" aria-label="Page navigation">
                <ul className="pagination pagination-lg">
                    {li}
                </ul>
            </nav>
        )
    }
}