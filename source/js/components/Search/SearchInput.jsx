import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { withTheme } from 'emotion-theming';
import withRef from '../../hocs/withRef';
import withTest from '../../hocs/withTest';

class SearchInput extends Component {
    static propTypes = {
        value: PropTypes.string,
        disabled: PropTypes.bool,
        
        onClick: PropTypes.func,
        onKeyUp: PropTypes.func,
        onChange: PropTypes.func
    }

    constructor() {
        super();
    }

    logTest = () => console.log(`SearchInput.logTest - test: ${this.props.testProp}`);

    render () {
        console.log(`SearchInput.render - test: ${this.props.testProp}`);
        console.log(`SearchInput.render - test ref: ${this.props.testPropRef}`);
        console.log(`SearchInput.render - theme`, this.props.theme);
        const { value, disabled, onClick, onKeyUp, onChange } = this.props;
        
        return (
            <div className="input-group">
                <input type="text" className="form-control" placeholder="Search for..." disabled={disabled} value={value} onChange={onChange} onKeyUp={onKeyUp}/>
                <span className="input-group-btn">
                <button className="btn btn-default" type="button" disabled={disabled} onClick={onClick}>Search</button>
                </span>
            </div>
        );
    }
}

export default compose(
    withTest(),
    withTheme,
    withRef(),
)(SearchInput);