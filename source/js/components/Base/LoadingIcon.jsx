import React, { Component } from 'react';

import iconImage from '../../../assets/img/gears.gif'

export default class LoadingIcon extends Component {
    constructor() {
        super();
    }

    render () {
        return (
            <img className="nasa-loading-icon" src={iconImage}/>
        );
    }
}