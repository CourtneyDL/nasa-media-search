import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class Navbar extends Component {
    render() {
        return (
            <nav className="navbar fixed-top navbar-light bg-light">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <NavLink
                            className='navbar-brand'
                            to={ `/` }
                            >NASA Search</NavLink>
                    </div>
                </div>
            </nav>
        );s
    }
}